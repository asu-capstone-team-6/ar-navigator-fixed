## Introduction

This repository contains code related to the AR Navigation Capstone project. 

This project seeks to make the task of students finding their way to classes 
simple via augmented reality paths leading to their destination.

This will be accopmlished by making an Android app, utilizing ARCore and Google 
Maps.

## Project Releases and Information

The current version of the project can be found
[here](https://drive.google.com/drive/folders/1Nc54AsTmo1svqP0R182oc_52hwFm5p3l?usp=sharing)!
You need to have [this](https://play.google.com/store/apps/details?id=com.google.ar.core)
app installed in order to run any ARCore applications. 

This project also has subsystem demos found on the 
[demos branch](https://gitlab.com/asu-capstone-team-6/ar-navigator/tree/demos).

Tutorials on how the subsystems work can be found in the 
[wiki](https://gitlab.com/asu-capstone-team-6/ar-navigator/wikis/home).
## Developers

Tyler Compton

Rahul Dawar

Paul Horton

Derek Koleber

Cecilia La Place
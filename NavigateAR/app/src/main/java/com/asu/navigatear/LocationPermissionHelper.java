package com.asu.navigatear;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.v13.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * Helper to ask for location permissions.
 */
public class LocationPermissionHelper {
    private static final int LOCATION_PERMISSION_CODE = 1;
    private static final String COARSE_LOCATION_PERMISSION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final String FINE_LOCATION_PERMISSION = Manifest.permission.ACCESS_FINE_LOCATION;

    /**
     * Check to see we have the necessary permissions for this app.
     */
    public static boolean hasLocationPermission(Activity activity) {
        return ContextCompat.checkSelfPermission(activity, COARSE_LOCATION_PERMISSION) ==
                        PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(activity, FINE_LOCATION_PERMISSION) ==
                        PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Check to see we have the necessary permissions for this app, and ask for them if we don't.
     */
    public static void requestLocationPermission(Activity activity) {
        ActivityCompat.requestPermissions(activity,
                new String[]{
                    COARSE_LOCATION_PERMISSION,
                    FINE_LOCATION_PERMISSION
                },
                LOCATION_PERMISSION_CODE);

    }

    /** Launch Application Setting to grant permission. */
    public static void launchPermissionSettings(Activity activity) {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setData(Uri.fromParts("package", activity.getPackageName(), null));
        activity.startActivity(intent);
    }
}

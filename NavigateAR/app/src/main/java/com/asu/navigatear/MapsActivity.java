package com.asu.navigatear;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.JointType;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {
    private GoogleMap mMap;
    private LocationManager locMan;
    private Hashtable<String,LatLng> places;
    private ArrayList<LatLng> tour_locations;
    private ArrayList<LatLng> path;
    private LatLng init_origin;
    private LatLng init_destination;
    private String init_destination_name;
    private Polyline path_line;
    private Polyline tour_line;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        Toolbar tbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(tbar);

        // Get a support ActionBar corresponding to this toolbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.gmap);
        mapFragment.getMapAsync(this);


        loadLocations();


        /*
        if(savedInstanceState.containsKey("destination_name")){
            init_destination_name = savedInstanceState.getString("destination_name");
        }
        if(savedInstanceState.containsKey("origin_lat") && savedInstanceState.containsKey("origin_lng")){
            init_origin = new LatLng(savedInstanceState.getDouble("origin_lat"),savedInstanceState.getDouble("origin_lng"));
        }

        init_destination_name = "santan";
        */

        init_origin = places.get("catalina");
        init_destination = places.get("sdfc");

        Button button1 = (Button) findViewById(R.id.arView_button);

        button1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //TextView text   = (TextView)findViewById(R.id.copyright);
                //dest1 = text.getText().toString();
                Intent map = new Intent(MapsActivity.this, CameraActivity.class);
                //map.putExtra("destination", dest1);
                startActivity(map);

            }
        });






    }

    @Override
    protected void onResume() {
        super.onResume();

        if (LocationPermissionHelper.hasLocationPermission(this)) {
            //set up location manager
            locMan = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locMan.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000,5,new LocationUpdater());

            Location actual_init_origin = locMan.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if(actual_init_origin != null){
                init_origin = new LatLng(actual_init_origin.getLatitude(),actual_init_origin.getLongitude());
            }

            getDirections(init_origin,init_destination, new DirectionsCallback(){
                public void onResponse(ArrayList<LatLng> resp){
                    path = resp;
                }
            });
        } else {
            LocationPermissionHelper.requestLocationPermission(this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] results) {
        if (!LocationPermissionHelper.hasLocationPermission(this)) {
            Toast.makeText(this,
                    "Location permission is needed to run this application", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    public void arView(View view){
        Intent intent = new Intent(this, CameraActivity.class);
        startActivity(intent);
    }

    public interface DirectionsCallback{
        void onResponse(ArrayList<LatLng> resp);
    }

    public void getDirections(LatLng origin, LatLng destination, final DirectionsCallback cb){
        RequestQueue q = Volley.newRequestQueue(this);
        String directions_request = "https://maps.googleapis.com/maps/api/directions/json?key=" +
            getString(R.string.google_directions_key) +
            "&origin=" + origin.latitude + "," + origin.longitude +
            "&destination=" + destination.latitude + "," + destination.longitude +
            "&mode=walking";
        //Log.i("api call",directions_request);
        StringRequest req = new StringRequest(Request.Method.GET, directions_request,
                new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {
                        //Log.i("api call", response);
                        ArrayList<LatLng> pathSteps = new ArrayList<LatLng>();
                        try {
                            JSONObject routeLocs = new JSONObject(response);
                            JSONObject leg = routeLocs.getJSONArray("routes").getJSONObject(0).getJSONArray("legs").getJSONObject(0);
                            JSONObject startPos = leg.getJSONObject("start_location");
                            pathSteps.add(new LatLng(startPos.getDouble("lat"),startPos.getDouble("lng")));

                            JSONArray path = leg.getJSONArray("steps");
                            for(int i = 0;i<path.length();i++){
                                JSONObject step = path.getJSONObject(i).getJSONObject("end_location");
                                pathSteps.add(new LatLng(step.getDouble("lat"),step.getDouble("lng")));
                            }
                        }catch(Exception e){}

                        cb.onResponse(pathSteps);
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError er){}
                }
        );
        q.add(req);
    }

    private double distanceBetween(LatLng a, LatLng b){
        return Math.sqrt(Math.pow(Math.abs(a.latitude-b.latitude),2)+Math.pow(Math.abs(a.longitude-b.longitude),2));
    }

    private Polyline plotPath(ArrayList<LatLng> locs,int col){
        PolylineOptions opts = new PolylineOptions()
                .jointType(JointType.ROUND)
                .width(10)
                .color(col);
        for(int i = 0;i<locs.size();i++){
            opts.add(locs.get(i));
        }
        return mMap.addPolyline(opts);
    }
    private void plotTourPath(){
        if(tour_locations == null || tour_locations.size() == 0){
            return;
        }
        tour_line = plotPath(tour_locations,Color.RED);
    }
    private void plotPathLive(LatLng current_pos){
        if(path == null || path.size() == 0){
            return;
        }
        if(path_line != null){
            path_line.remove();
        }
        //find closest point on the path
        int closest_ind = 0;
        double closest_dist = -1;
        for(int i = 0;i<path.size();i++ ){
            double dist = distanceBetween(path.get(i),current_pos);
            if(closest_dist == -1 || dist < closest_dist){
                closest_ind = i;
                closest_dist = dist;
            }
        }
        ArrayList<LatLng> path_locs = new ArrayList<LatLng>();
        if(path.size() != 0) {
            path_locs.addAll(closest_ind, path);
        }
        path_locs.add(0,current_pos);
        path_line = plotPath(path_locs,Color.BLACK);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        try{
            mMap = googleMap;
        }
        catch(Exception e){}

        plotTourPath();

        CameraPosition camPos = new CameraPosition.Builder()
            .target(init_origin)
            .zoom(18)
            .build();
        CameraUpdate cupd = CameraUpdateFactory.newCameraPosition(camPos);
        mMap.animateCamera(cupd);
        //Log.i("testerino",init_origin.toString());
        //plotPathLive(init_origin);
    }

    class LocationUpdater implements LocationListener{
        public void onLocationChanged(Location location){
            Log.i("location","location has changed");
            LatLng loc_pos = new LatLng(location.getLatitude(),location.getLongitude());

            CameraPosition cam_pos = new CameraPosition.Builder()
                .target(loc_pos)
                .zoom(18)
                .build();
            CameraUpdate cam_up = CameraUpdateFactory.newCameraPosition(cam_pos);
            mMap.animateCamera(cam_up);
            plotPathLive(loc_pos);
        }
        public void onStatusChanged(String provider,int status, Bundle extras){}
        public void onProviderDisabled(String provider){}
        public void onProviderEnabled(String provider){}
    }
    private LatLng jsonLatLng(JSONObject o) throws JSONException{
        return new LatLng(o.getDouble("lat"),o.getDouble("lng"));
    }
    private void loadLocations(){
        InputStream is = getResources().openRawResource(R.raw.locations);
        StringWriter writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        try{
            is.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        //Hashtable<String,LatLng> res = new  Hashtable<String,LatLng>();
        try{
            JSONObject all_data = new JSONObject(writer.toString());
            JSONObject locations = all_data.getJSONObject("places");
            Iterator<String> locIter = locations.keys();
            places = new Hashtable<String,LatLng>();
            while(locIter.hasNext()){
                String locName = locIter.next();
                JSONObject currLoc = locations.getJSONObject(locName);
                places.put(locName, jsonLatLng(currLoc));
            }
            tour_locations = new ArrayList<LatLng>();
            JSONArray tour_locs = all_data.getJSONObject("points of interest").getJSONArray("tour");
            for(int i = 0;i< tour_locs.length();i++){
                tour_locations.add(jsonLatLng(tour_locs.getJSONObject(i)));
            }

        }
        catch(Exception e){
            e.printStackTrace();
        }
        //return res;

    }
}

package com.asu.navigatear.rendering;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;

import com.asu.navigatear.R;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

/**
 * Renders a "line" between two points. In actuality, it's a rectangle.
 */
public class LineRenderer {
    private static final String TAG = LineRenderer.class.getSimpleName();

    private static final int COORDS_PER_VERTEX = 3;
    private static final int VERTEX_STRIDE = COORDS_PER_VERTEX * 4;
    private static final int BYTES_PER_FLOAT = 4;
    private static final int BYTES_PER_SHORT = 2;

    private static final float LINE_WIDTH = 0.02f;

    private float coords[] = {
            // Points further from user
            -0.5f, +0.5f, +0.5f, // Top left
            -0.5f, -0.5f, +0.5f, // Bottom left
            +0.5f, -0.5f, +0.5f, // Bottom right
            +0.5f, +0.5f, +0.5f, // Top right

            // Points closer to the user
            -0.5f, +0.5f, -0.5f, // Top left
            -0.5f, -0.5f, -0.5f, // Bottom left
            +0.5f, -0.5f, -0.5f, // Bottom right
            +0.5f, +0.5f, -0.5f, // Top right
    };

    private static final short DRAW_ORDER[] = {
            0, 1, 2,  0, 2, 3,
            3, 2, 6,  3, 6, 7,
            4, 5, 6,  4, 6, 7,
            0, 1, 5,  0, 5, 4,
            4, 0, 3,  4, 3, 7,
            5, 1, 2,  5, 2, 6
    };

    private static final float color[] = {1.0f, 0.0f, 0.0f, 1.0f};

    private int program;

    private int mvpMatrixUniformId;
    private int positionAttributeId;
    private int colorUniformId;

    private FloatBuffer vertexBuffer;
    private ShortBuffer drawOrderBuffer;

    public void createOnGlThread(Context context) {
        // Create a buffer to put coordinates in. We need to start with a raw
        // ByteBuffer first so that we can set the endianness of the buffer.
        // OpenGL requires native order.
        ByteBuffer bb = ByteBuffer.allocateDirect(coords.length * BYTES_PER_FLOAT);
        bb.order(ByteOrder.nativeOrder());
        vertexBuffer = bb.asFloatBuffer();
        vertexBuffer.put(coords);
        vertexBuffer.position(0);

        // Create a buffer to put draw order in. See above comment for details
        bb = ByteBuffer.allocateDirect(DRAW_ORDER.length * BYTES_PER_SHORT);
        bb.order(ByteOrder.nativeOrder());
        drawOrderBuffer = bb.asShortBuffer();
        drawOrderBuffer.put(DRAW_ORDER);
        drawOrderBuffer.position(0);

        // Set up the shader
        program = GLES20.glCreateProgram();
        int vertexShader = ShaderUtil.loadGLShader(TAG, context, GLES20.GL_VERTEX_SHADER,
                R.raw.line_vertex);
        int fragmentShader = ShaderUtil.loadGLShader(TAG, context, GLES20.GL_FRAGMENT_SHADER,
                R.raw.line_fragment);
        GLES20.glAttachShader(program, vertexShader);
        GLES20.glAttachShader(program, fragmentShader);
        ShaderUtil.checkGLError(TAG, "After attaching shaders");
        GLES20.glLinkProgram(program);

        ShaderUtil.checkGLError(TAG, "After program linking");
    }

    public void setVertices(float x1, float y1, float z1, float x2, float y2, float z2) {
        coords[0] = x1 - LINE_WIDTH;
        coords[1] = y1 + LINE_WIDTH;
        coords[2] = z1 - LINE_WIDTH;

        coords[3] = x1 + LINE_WIDTH;
        coords[4] = y1 + LINE_WIDTH;
        coords[5] = z1 + LINE_WIDTH;

        coords[6] = x2 + LINE_WIDTH;
        coords[7] = y2 + LINE_WIDTH;
        coords[8] = z2 + LINE_WIDTH;

        coords[9] = x2 - LINE_WIDTH;
        coords[10] = y2 + LINE_WIDTH;
        coords[11] = z2 - LINE_WIDTH;

        coords[12] = x1 - LINE_WIDTH;
        coords[13] = y1 - LINE_WIDTH;
        coords[14] = z1 - LINE_WIDTH;

        coords[15] = x1 + LINE_WIDTH;
        coords[16] = y1 - LINE_WIDTH;
        coords[17] = z1 + LINE_WIDTH;

        coords[18] = x2 + LINE_WIDTH;
        coords[19] = y2 - LINE_WIDTH;
        coords[20] = z2 + LINE_WIDTH;

        coords[21] = x2 - LINE_WIDTH;
        coords[22] = y2 - LINE_WIDTH;
        coords[23] = z2 - LINE_WIDTH;

        vertexBuffer.put(coords);
        vertexBuffer.position(0);
    }

    public void draw(float[] cameraViewMatrix, float[] cameraPerspectiveMatrix) {
        ShaderUtil.checkGLError(TAG, "Before draw");

        // Apply the camera view and perspective matrices
        float[] matrix = new float[16];
        Matrix.setIdentityM(matrix, 0);
        Matrix.multiplyMM(matrix, 0, cameraViewMatrix, 0, matrix, 0);
        Matrix.multiplyMM(matrix, 0, cameraPerspectiveMatrix, 0, matrix, 0);

        GLES20.glUseProgram(program);

        positionAttributeId = GLES20.glGetAttribLocation(program, "vPosition");
        GLES20.glEnableVertexAttribArray(positionAttributeId);

        GLES20.glVertexAttribPointer(positionAttributeId, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                VERTEX_STRIDE, vertexBuffer);

        colorUniformId = GLES20.glGetUniformLocation(program, "vColor");
        GLES20.glUniform4fv(colorUniformId, 1, color, 0);

        mvpMatrixUniformId = GLES20.glGetUniformLocation(program, "uMVPMatrix");
        GLES20.glUniformMatrix4fv(mvpMatrixUniformId, 1, false, matrix, 0);

        GLES20.glDrawElements(GLES20.GL_TRIANGLES, DRAW_ORDER.length,
                GLES20.GL_UNSIGNED_SHORT, drawOrderBuffer);

        GLES20.glDisableVertexAttribArray(positionAttributeId);

        ShaderUtil.checkGLError(TAG, "After draw");
    }
}

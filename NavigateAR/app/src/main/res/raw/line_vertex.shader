// An input, the transformation matrix for the object. Fed once per object
uniform mat4 uMVPMatrix;

// An input, individual points of the object. Fed once per vertex in object.
attribute vec4 vPosition;

void main() {
    gl_Position = uMVPMatrix * vPosition;
}
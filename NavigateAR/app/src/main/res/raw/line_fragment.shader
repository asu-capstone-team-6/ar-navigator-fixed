precision mediump float;

// An input, the color of the fragment
uniform vec4 vColor;

void main() {
    gl_FragColor = vColor;
}